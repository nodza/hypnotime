//
//  HypnosisView.h
//  Hypnosister
//
//  Created by Noel Hwande on 6/29/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface HypnosisView : UIView
{
    NSMutableArray *randomColors;
    CALayer *boxLayer;
}

@property (nonatomic, strong) UIColor *circleColor;

@end
