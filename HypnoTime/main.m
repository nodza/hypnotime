//
//  main.m
//  HypnoTime
//
//  Created by Noel Hwande on 7/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HypnosisterAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HypnosisterAppDelegate class]));
    }
}
