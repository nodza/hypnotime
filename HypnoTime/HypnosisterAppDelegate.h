//
//  HypnosisterAppDelegate.h
//  HypnoTime
//
//  Created by Noel Hwande on 7/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HypnosisterAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
