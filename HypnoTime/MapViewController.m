//
//  MapViewController.m
//  HypnoTime
//
//  Created by Noel Hwande on 7/3/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "MapViewController.h"

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    // Call the superclass' designated initializer
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        // Get the tab bar item
        UITabBarItem *tbi = [self tabBarItem];
        
        UIImage *i = [UIImage imageNamed:@"location.png"];
        
        [tbi setImage:i];
        [tbi setTitle:@"Map"];
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"MapViewController loaded its view.");
    
}

@end
