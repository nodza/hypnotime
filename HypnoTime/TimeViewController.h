//
//  TimeViewController.h
//  HypnoTime
//
//  Created by Noel Hwande on 7/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeViewController : UIViewController
{
  __weak IBOutlet UILabel *timeLabel;
}
- (IBAction)showCurrentTime:(id)sender;

@end
