//
//  main.m
//  Hypnosister
//
//  Created by Noel Hwande on 6/29/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HypnosisterAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HypnosisterAppDelegate class]));
    }
}
