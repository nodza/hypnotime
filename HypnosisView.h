//
//  HypnosisView.h
//  Hypnosister
//
//  Created by Noel Hwande on 6/29/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HypnosisView : UIView
{
    NSMutableArray *randomColors;
}

@property (nonatomic, strong) UIColor *circleColor;

@end
